name             'node-tar'

depends          "tar"

%w(ubuntu).each do |os|
  supports os
end
