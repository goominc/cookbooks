tar_extract "https://nodejs.org/dist/v#{node['node-tar']['version']}/node-v#{node['node-tar']['version']}-#{node['node-tar']['suffix']}.tar.gz" do
  target_dir '/usr/local/lib'
end

link "/usr/local/bin/node" do
  to "/usr/local/lib/node-v#{node['node-tar']['version']}-#{node['node-tar']['suffix']}/bin/node"
end

link "/usr/local/bin/npm" do
  to "/usr/local/lib/node-v#{node['node-tar']['version']}-#{node['node-tar']['suffix']}/bin/npm"
end
