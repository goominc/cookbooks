tar_extract "https://s3-ap-northeast-1.amazonaws.com/node-harmony/node-v#{node['node-harmony']['version']}-harmony.tar.gz" do
  target_dir '/usr/local/lib'
end

link "/usr/local/bin/node" do
  to "/usr/local/lib/node-v#{node['node-harmony']['version']}-harmony/bin/node"
end

link "/usr/local/bin/npm" do
  to "/usr/local/lib/node-v#{node['node-harmony']['version']}-harmony/bin/npm"
end
